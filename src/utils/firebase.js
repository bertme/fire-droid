import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyBQa3p5S3t1aI3xbcnyvaOqNfLiEOlgyPc',
  authDomain: 'firedroid-da9e3.firebaseapp.com',
  databaseURL: 'https://firedroid-da9e3.firebaseio.com',
  projectId: 'firedroid-da9e3',
  storageBucket: 'firedroid-da9e3.appspot.com',
  messagingSenderId: '945852556324'
};

const firebaseApp = firebase.initializeApp(config)

export default firebaseApp
