import firebase from 'firebase'
import 'firebase/firestore'
import firebaseApp from '../utils/firebase'

export async function getExams() {
  const collection = await firebase.firestore(firebaseApp).collection('ExamPaper').get()
  const exams = []
  collection.forEach((doc) => {
    exams.push({
      id: doc.id,
      ...doc.data()
    })
  })
  return new Promise((resolve) => {
    resolve({ data: exams })
  })
}
