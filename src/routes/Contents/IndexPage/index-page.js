import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'

import ExamCard from '../../../components/ExamCard'
import styles from './index-page.less'

class IndexPage extends React.Component {
  get exams() {
    return this.props.exams.map(exam => (
      <ExamCard {...exam} key={exam.id} />
    ))
  }
  render() {
    return (
      <div className={styles.list}>
        <div className={styles.head}>
          <div>试卷名称</div>
          <div>测试状态</div>
        </div>
        <ul className={styles.list}>
          {this.exams}
        </ul>
      </div>
    )
  }
}

IndexPage.defaultProps = {
  exams: []
}

IndexPage.propTypes = {
  exams: PropTypes.array
}

const mapStateToProps = state => ({
  exams: state.indexPage.exams
})

export default connect(mapStateToProps)(IndexPage)
