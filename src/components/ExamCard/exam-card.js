import React from 'react';
import PropTypes from 'prop-types';

import styles from './exam-card.less'

const titleStyle = {
  fontSize: 18,
  fontWeight: 600,
  color: '#2B2B2B'
}

const ExamCard = ({ title }) => {
  return (
    <div className={styles.card}>
      <div
        className="title"
        style={titleStyle}
      >
        { title }
      </div>
      <a href className={styles['primary-text']}>开始答题</a>
    </div>
  );
};

ExamCard.defaultProps = {
  title: 'No Title'
}

ExamCard.propTypes = {
  title: PropTypes.string
}

export default ExamCard;
