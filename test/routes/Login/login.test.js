import expect from 'expect'
import {mapStateToProps} from '../../../src/routes/Login/login'
describe('login', function () {
  const state = {
    login: {
      userName: 'userName',
      phoneNumber: '13800138000'
    },
    loading:{
      models:{}
    },
    app:{}
  }
  it('should map userName from state to props', function () {
    const props = mapStateToProps(state)
    expect(props.userName).toEqual('userName')
  })
  it('should map phoneNumber from state to props', function () {
    const props = mapStateToProps(state)
    expect(props.phoneNumber).toEqual('13800138000')
  })
})
