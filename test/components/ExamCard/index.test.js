import React from 'react'
import { expect } from 'chai'
import { shallow } from 'enzyme'

import ExamCard from '../../../src/components/ExamCard/exam-card'

describe('<ExamCard />', () => {
  it('should render a div', () => {
    const component = shallow(<ExamCard />)
    expect(component.type()).to.equal('div')
  })
  it('should contain a title', () => {
    const component = shallow(<ExamCard />)
    expect(component.children().first().hasClass('title')).to.equal(true)
  })
  it('should render the correct title', () => {
    const testTitle = '标题'
    const componentProps = { title: testTitle }
    const component = shallow(<ExamCard {...componentProps}/>)
    expect(component.children().first().text()).to.equal(testTitle)
  })
  it('should contain a link for answering', () => {
    const component = shallow(<ExamCard />)
    expect(component.find('a').length).to.equal(1)
  })
})
